﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Net.Sockets;
using System.IO;
using System.Timers;

namespace PADIMapNoReduce
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                PuppetMasterForm puppetMasterForm = new PuppetMasterForm();
                Application.Run(puppetMasterForm);
            }
            finally
            {
                string takenports = @"takenports.txt";
                TryToDelete(takenports);
            }
        }

        static bool TryToDelete(string f)
        {
            try
            {
                // Try to delete the file.
                File.Delete(f);
                return true;
            }
            catch (IOException)
            {
                // We could not delete the file.
                return false;
            }
        }
    }

    public delegate void Logger(String log);
    public class PuppetMasterControls : MarshalByRefObject, IPuppetMasterService
    {
        // My connection
        private TcpChannel channel;
        String myUrl;

        // Script
        private String[] script;
        private int currentStep;
        // Log Output
        private PuppetMasterForm puppetMasterForm;

        // Worker nodes
        private IDictionary<int, IWorkerService> workers = new Dictionary<int, IWorkerService>();

        public PuppetMasterControls(PuppetMasterForm puppetMasterForm)
        {
            this.puppetMasterForm = puppetMasterForm;

            int freeport;
            string takenports = @"takenports.txt";

            //if first puppetmaster
            if (!File.Exists(takenports))
            {
                freeport = 20001;
            }
            // if is not the first puppetmaster
            else
            {
                string text = System.IO.File.ReadAllText(takenports);
                freeport = Convert.ToInt32(text);
                freeport++;
            }
            System.IO.File.WriteAllText(takenports, (freeport).ToString());

            channel = new TcpChannel(freeport);
            ChannelServices.RegisterChannel(channel, true);
            myUrl = "tcp://localhost:" + freeport + "/PM";
            RemotingServices.Marshal(this, myUrl, typeof(IPuppetMasterService));
        }

        public void RunCommand(String command)
        {
            // if comment, ignore
            if (isComment(command))
            {
                return;
            }

            String[] args = command.Split(' ');
            switch (args[0])
            {
                case "WORKER":
                    {
                        int id = Int32.Parse(args[1]);

                        String puppetMasterUrl = args[2], serviceUrl = args[3], entryURL;
                        // check if entryURL is provided
                        if (args.Length < 5)
                        {
                            entryURL = "";
                        }
                        else
                        {
                            entryURL = args[4];
                        }

                        if (puppetMasterUrl.Equals(myUrl))
                        {
                            createNewWorker(id, serviceUrl, entryURL);
                        }
                        else
                        {
                            IPuppetMasterService otherPuppetMaster = (IPuppetMasterService)Activator.GetObject(
                                                                    typeof(IPuppetMasterService),
                                                                    puppetMasterUrl);
                            otherPuppetMaster.createNewWorker(id, serviceUrl, entryURL);
                        }

                        // wait a bit before continuing in order to allow the worker to initialize itself
                        System.Threading.Thread.Sleep(250);
                    }
                    break;
                case "SUBMIT":
                    {
                        String entryURL = args[1], file = args[2], output = args[3], mapName = args[5], mapImpl = args[6];
                        int s = Int32.Parse(args[4]);

                        Process.Start(@"..\..\..\Client\bin\Debug\Client.exe", entryURL + " " + file + " " + output + " " + s + " " + mapName + " " + mapImpl);
                        WriteLog("Job Submitted.");
                    }
                    break;
                case "WAIT":
                    {
                        int secs = Int32.Parse(args[1]);
                        WriteLog("Waiting For " + secs + " seconds...");
                        System.Threading.Thread.Sleep(secs * 1000);
                        WriteLog("Stoped waiting. Running Application again");
                    }
                    break;
                case "STATUS":
                    {
                        WriteLog("Ordering Workers to print their status...");
                        foreach (IWorkerService worker in workers.Values)
                        {
                            worker.PrintStatus();
                        }
                        WriteLog("All status printed");
                    }
                    break;
                case "SLOWW":
                    {
                        int id = Int32.Parse(args[1]), delayInSeconds = Int32.Parse(args[2]);
                        IWorkerService worker;
                        if (workers.TryGetValue(id, out worker))
                        {
                            WriteLog("Slowing worker #" + id + " for " + delayInSeconds + " seconds...");
                            worker.SlowW(delayInSeconds);
                        }
                        else
                        {
                            WriteLog("ERROR: Worker #" + id + " not found.");
                        }
                    }
                    break;
                case "FREEZEW":
                    {
                        int id = Int32.Parse(args[1]);
                        IWorkerService worker;
                        if (workers.TryGetValue(id, out worker))
                        {
                            WriteLog("Freezing worker #" + id + ".");
                            worker.FreezeW();
                        }
                        else
                        {
                            WriteLog("ERROR: Worker #" + id + " not found.");
                        }
                    }
                    break;
                case "UNFREEZEW":
                    {
                        int id = Int32.Parse(args[1]);
                        IWorkerService worker;
                        if (workers.TryGetValue(id, out worker))
                        {
                            WriteLog("Unfreezing worker #" + id + ".");
                            worker.UnfreezeW();
                        }
                        else
                        {
                            WriteLog("ERROR: Worker #" + id + " not found.");
                        }
                    }
                    break;
                case "FREEZEC":
                    {
                        int id = Int32.Parse(args[1]);
                        IWorkerService worker;
                        if (workers.TryGetValue(id, out worker))
                        {
                            WriteLog("Freezing Job Tracker #" + id + ".");
                            worker.FreezeC();
                        }
                        else
                        {
                            WriteLog("ERROR: Job Tracker #" + id + " not found.");
                        }
                    }
                    break;
                case "UNFREEZEC":
                    {
                        int id = Int32.Parse(args[1]);
                        IWorkerService worker;
                        if (workers.TryGetValue(id, out worker))
                        {
                            WriteLog("Freezing Job Tracker #" + id + ".");
                            worker.FreezeW();
                        }
                        else
                        {
                            WriteLog("ERROR: Job Tracker #" + id + " not found.");
                        }
                    }
                    break;
                default:
                    {
                        WriteLog("Command '" + args[0] + "' Not Found.");
                    }
                    break;
            }
        }

        private bool isComment(String command)
        {
            return command[0] == '%';
        }

        private void WriteLog(String log)
        {
            puppetMasterForm.Invoke(new Logger(puppetMasterForm.WriteLog), new Object[] { log });
        }

        public void LoadFile(String file)
        {
            script = File.ReadAllLines(file);
            currentStep = 0;
            WriteLog("File '" + file + "' loaded.");
        }

        public void RunScript()
        {
            for (/* NO INIT */; currentStep < script.Length; currentStep++)
            {
                RunCommand(script[currentStep]);
            }
            WriteLog("Script Finished.");
        }

        public void StepScript()
        {
            // Skip all comments
            while (currentStep < script.Length && isComment(script[currentStep]))
            {
                currentStep++;
            }

            // Check if we are at the end ofthe script
            if (currentStep >= script.Length)
            {
                WriteLog("Couldn't Step. Reached end of script.");
                return;
            }

            RunCommand(script[currentStep++]);
        }

        public void createNewWorker(int id, string serviceUrl, string entryURL)
        {
            Process.Start(@"..\..\..\Worker\bin\Debug\Worker.exe", serviceUrl + " " + entryURL);

            IWorkerService worker = (IWorkerService)Activator.GetObject(
                typeof(IWorkerService),
                serviceUrl);
            workers.Add(id, worker);
            WriteLog("Novo Worker #" + id + " no Url " + serviceUrl + " criado.");
        }
    }
}
